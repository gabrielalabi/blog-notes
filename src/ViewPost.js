import React, { useEffect, useState } from "react";
import SideBar from "./components/SideBar";
import NavBar from "./components/NavBar";
import { useParams } from "react-router-dom";
import axios from "axios";
import DisplayPosts from "./components/DisplayPosts";
const ViewPost = () => {
  const [viewPost, setViewPost] = useState();
  const { id } = useParams();
  useEffect(() => {
    const fetchPost = async () => {
      const res = await axios.get(
        `https://jsonplaceholder.typicode.com/posts/${id}`
      );
      setViewPost(res.data);
    };
    fetchPost();
  }, []);
  if (!viewPost) return "cargando...";
  const posts = {
    title: viewPost.title,
    body: viewPost.body,
  };
  return (
    <>
      <NavBar />
      <div className="w-100 d-flex flex-row">
        <SideBar />
        <DisplayPosts posts={posts} />
      </div>
    </>
  );
};

export default ViewPost;
