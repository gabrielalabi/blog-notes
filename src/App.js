import MainPage from './MainPage';
import './index.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import ViewPost from './ViewPost';
import EditPost from './EditPost';
import Login from './Login';
import CreatePost from './CreatePost';

function App() {
  const isLoggedIn = () => {
    localStorage.getItem('token');
  };
  return (
<div>
<Router>
    <Switch>
          <Route path="/post/:id">
            {isLoggedIn ? <ViewPost /> : <Redirect to='/login'/> } 
          </Route>
          <Route path="/editpost/:id">
           {isLoggedIn ? <EditPost /> : <Redirect to="login" />} 
          </Route>
          <Route path="/login">
           <Login/> 
          </Route>
          <Route path="/newPost">
          {isLoggedIn ? <CreatePost/> : <Redirect to='/login'/> }  
          </Route>
          <Route exact path="/">
          {isLoggedIn ? <MainPage/> : <Redirect to="/login"/> }
          </Route>
    </Switch>
    </Router>
    </div>
    
  );
}

export default App;
