import SideBar from "./components/SideBar";
import NavBar from "./components/NavBar";
import DisplayPosts from "./components/DisplayPosts";
const MainPage = () => {
  const posts = {
    title: "Welcome to Blognotes",
    body: "Search here your favourite story.",
  };
  return (
    <>
      <NavBar />
      <div className="w-100 d-flex flex-row">
        <SideBar />
        <DisplayPosts posts={posts} />
      </div>
    </>
  );
};

export default MainPage;
