import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import SideBar from "./components/SideBar";
import NavBar from "./components/NavBar";
import Swal from "sweetalert2";
const EditPost = () => {
  const history = useHistory();
  const { id } = useParams();
  const [postToEdit, setPostToEdit] = useState();
  useEffect(() => {
    const fetchPost = async () => {
      const res = await axios.get(
        `https://jsonplaceholder.typicode.com/posts/${id}`
      );
      setPostToEdit(res.data);
    };
    fetchPost();
  }, []);
  const schemaValidacion = Yup.object({
    title: Yup.string().required("Title is mandatory"),
    body: Yup.string().required("Your post cannot go empty"),
  });
  const updatePost = async (values) => {
    await axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      title: values.title,
      body: values.body,
    });
    Swal.fire("Good job!", "You updated your post!", "success");
    history.push("/");
  };
  if (!postToEdit) return "cargando...";
  return (
    <>
      <NavBar />
      <div className="w-100 d-flex flex-row">
        <SideBar className="w-25" />
        <div className="w-100  text-white  d-flex align-items-center">
          <Formik
            validationSchema={schemaValidacion}
            enableReinitialize
            initialValues={postToEdit}
            onSubmit={(values) => {
              updatePost(values);
            }}
          >
            {(props) => {
              return (
                <form
                  className="d-flex flex-column align-items-center bg-dark"
                  onSubmit={props.handleSubmit}
                >
                  <label
                    className="block text-white h2  fw-bold mb-2"
                    htmlFor="title"
                  >
                    Title
                  </label>
                  {props.touched.title && props.errors.title ? (
                    <div className="my-2 bg-danger w-100 text-center">
                      <p className="fw-bold">Error</p>
                      <p>{props.errors.title}</p>
                    </div>
                  ) : null}
                  <input
                    className="border rounded px-4 pt-2 w-50 text-center "
                    id="title"
                    type="title"
                    placeholder="title of the post"
                    onChange={props.handleChange}
                    value={props.values.title}
                  />
                  <label
                    className="block text-white h2  fw-bold mt-5 mb-1"
                    htmlFor="body"
                  >
                    Content of the post
                  </label>
                  {props.touched.body && props.errors.body ? (
                    <div className="mb-2 bg-danger w-100 text-center">
                      <p className="fw-bold">Error</p>
                      <p>{props.errors.body}</p>
                    </div>
                  ) : null}
                  <textarea
                    rows="5"
                    cols="80"
                    className="border rounded  py-2 px-5 w-75 mb-4"
                    id="body"
                    type="body"
                    placeholder="Body"
                    onChange={props.handleChange}
                    value={props.values.body}
                  />
                  <input
                    type="submit"
                    className="  btn btn-primary mt-8 border border-secondary border-4  font-semibold  py-2 px-4 mb-3 border  rounded"
                    value="Modify Post"
                  />
                </form>
              );
            }}
          </Formik>
        </div>
      </div>
    </>
  );
};

export default EditPost;
