import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import SideBar from "./components/SideBar";
import NavBar from "./components/NavBar";
import Swal from "sweetalert2";
const EditPost = () => {
  const history = useHistory();
  const { id } = useParams();
  const [postToEdit, setPostToEdit] = useState();
  const formik = useFormik({
    initialValues: {
      title: "",
      body: "",
    },
    validationSchema: Yup.object({
      title: Yup.string().required("Title is mandatory"),
      body: Yup.string().required("Your post cannot go empty"),
    }),
    onSubmit: async (values) => {
      await axios.post(`https://jsonplaceholder.typicode.com/posts/`, {
        title: values.title,
        body: values.body,
      });
      Swal.fire("Good job!", "You created your post!", "success");
      history.push("/");
    },
  });
  return (
    <>
      <NavBar />
      <div className="w-100 d-flex flex-row">
        <div className="w-100 d-flex flex-row">
          <SideBar />
          <div className="w-100  text-white  d-flex align-items-center">
            <form
              className="d-flex flex-column align-items-center bg-dark "
              onSubmit={formik.handleSubmit}
            >
              <label
                className="block text-white h2  fw-bold mb-2"
                htmlFor="title"
              >
                Title
              </label>
              {formik.touched.title && formik.errors.title ? (
                <div className="my-2 bg-danger w-100 text-center">
                  <p className="fw-bold">Error</p>
                  <p>{formik.errors.title}</p>
                </div>
              ) : null}
              <input
                className=" border rounded  py-2 w-50 text-center "
                id="title"
                type="title"
                placeholder="Title of the post"
                onChange={formik.handleChange}
                value={formik.values.title}
              />

              <label
                className="block text-white h2  fw-bold mt-5 mb-1"
                htmlFor="body"
              >
                Content of the post
              </label>
              {formik.touched.body && formik.errors.body ? (
                <div className="my-2  bg-danger w-100 text-center">
                  <p className="fw-bold">Error</p>
                  <p>{formik.errors.body}</p>
                </div>
              ) : null}

              <textarea
                rows="5"
                cols="80"
                className=" border rounded  py-2 px-5 w-75 mb-4"
                id="body"
                type="body"
                placeholder="Body"
                onChange={formik.handleChange}
                value={formik.values.body}
              />
              <input
                type="submit"
                className="  btn btn-primary mt-8 border border-secondary border-4  font-semibold  py-2 px-4 mb-3 border  rounded"
                value="Create Post"
              />
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditPost;
