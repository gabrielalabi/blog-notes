const DisplayPosts = ({posts}) => {
    const {title,body} = posts;
    return ( 
        <div className="w-100 d-flex align-item-xenter justify-content-center text-white container">
        <div className="h1  w-100 h-100  text-white">
          <h1 className="h1 bg-dark mt-5 text-center rounded text-uppercase"> {title} </h1>
          <div className="h3 background-body rounded p-5"> <p className="text-black bg-light shadow-generated p-2 text-justify"> {body} </p> </div>
          </div>
        </div>

     );
}
 
export default DisplayPosts;