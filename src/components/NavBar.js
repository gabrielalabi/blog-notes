import {useHistory} from 'react-router-dom';
const Navbar = () => {
const history = useHistory();
    return ( 
   <nav className="navbar  navbar-dark bg-dark pl-4">
  <a className="ml-4" onClick={() => history.push('/')}>
  <img src='/images/logo.gif'  className="mb-2 pl-4 "/>
  </a>
</nav>
     );
}
 
export default Navbar;