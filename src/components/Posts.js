import React from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";
const Posts = ({ posts, loading }) => {
  let history = useHistory();
  if (loading) {
    return <h2>Loading...</h2>;
  }
  const viewPost = (id) => {
    history.push(`/post/${id}`);
    window.location.reload();
  };
  const deletePost = (id) => {
    Swal.fire({
      title: "Are you sure you want to delete this post?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const deletePost = await axios.delete(
          `https://jsonplaceholder.typicode.com/posts/${id}`
        );
        console.log(deletePost);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  };
  const editPost = (id) => {
    history.push(`/editpost/${id}`);
    window.location.reload();
  };
  return (
    <div className="bg-light  w-100">
      {posts.map((post) => (
        <div className="container">
          <div
            key={post.id}
            className=" d-flex flex-row w-100 justify-content-between"
          >
            <p
              className=" col-10  font-weight-bold cursor-pointer h5 cursor-pointer"
              onClick={() => viewPost(post.id)}
            >
              {" "}
              {post.title}{" "}
            </p>
            <div className="col-2  d-flex flex-row  ">
              <div onClick={() => deletePost(post.id)}>
                {" "}
                <svg
                  className="w-100  cursor-pointer"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                    clip-rule="evenodd"
                  ></path>
                </svg>{" "}
              </div>
              <div onClick={() => editPost(post.id)}>
                {" "}
                <svg
                  class="w-100 cursor-pointer"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z"></path>
                  <path
                    fill-rule="evenodd"
                    d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z"
                    clip-rule="evenodd"
                  ></path>
                </svg>{" "}
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Posts;
