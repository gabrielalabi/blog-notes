import React,{useState, useEffect} from 'react';
import PostList from "./PostList";
import { useHistory } from "react-router-dom";

const SideBar = () => {
  const history = useHistory();
  const logOut = () => {
    localStorage.removeItem("token");
    setTimeout(() => {
      history.push("/login");
    }, 1500);
  };
  const [bar, setBar] = useState(true);
  let barClass = "";
  useEffect(() => {
    if (bar == true) {
      barClass.replace(
        "bar-lateral",
        "bar-activa"
      );
    }
    if (bar == false) {
      barClass = "bar-lateral";
    }
  });
  return (
    <div>
    <div onClick={() => setBar(!bar)} className="burguer">
        <div className={bar ? "line1" : "line1b"}></div>
        <div className={bar ? "line2" : "line2b"}></div>
        <div className={bar ? "line3" : "line3b"}></div>
      </div>
      <aside
        className={
          bar
            ? "bar-lateral"
            : "bar-active"
        }
        id="bar"
      >
      <div className="d-flex flex-row justify-content-around bg-dark w-75">
        <div className="cursor-pointer">
          <div
            className="cursor-pointer"
            onClick={() => history.push("/newPost")}
          >
            <span className="h1 ml-5 sideBar-link"> Create Post </span>
          </div>
        </div>
        <div className="cursor-pointer">
          <p className="cursor-pointer" onClick={() => logOut()}>
            <span className="h1 ml-5 sideBar-link"> Log Out </span>
          </p>
        </div>
      </div>
      <PostList />
      </aside>
    </div>
  );
};

export default SideBar;
